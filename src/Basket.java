import java.util.ArrayList;


public class Basket {
    private ArrayList<Product> product = new ArrayList<>();
    private int amount;
    private double total;

    public Basket() {
    }

    public Basket(ArrayList<Product> product, int amount, double total) {
        this.product = product;
        this.amount = amount;
        this.total = total;
    }

    public ArrayList<Product> getProduct() {
        return product;
    }

    public void addProduct(Product product) {
        this.product.add(product);
    }
    public void deleteProduct(Product product) {
        this.product.remove(product);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}