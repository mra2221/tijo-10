import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BasketTest {
    private Shop shop;

    @BeforeEach
    public void init(){
        shop = new Shop();
    }
    @Test
    public void checkBasket() {
        Product pc1 = new Product("pc1",1500);
        Product pc2 = new Product("pc2",1800);
        assertEquals(1, shop.addProduct(pc1),"add pc1");
        assertEquals(2, shop.addProduct(pc2),"add pc2");
        assertEquals(3, shop.addProduct(pc2),"add pc2");

        assertEquals(2, shop.deleteProduct(pc2), "remove pc2");
        assertEquals(2, shop.countProducts(),"count 2");
        assertEquals(3300, shop.getTotalPrice(),"price total");
        assertEquals(1500, shop.getPrice("pc1"),"price pc1");

    }
}