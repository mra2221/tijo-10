

public class Shop {
    private Basket basket = new Basket();

    public int addProduct(Product product) {
        basket.setTotal(basket.getTotal() + product.getPrice());
        basket.setAmount(basket.getAmount() + 1);
        basket.addProduct(product);
        return basket.getAmount();
    }


    public int deleteProduct(Product product) {

        for (Product p : basket.getProduct()) {
            if (p.equals(product)) {
                basket.setAmount(basket.getAmount() - 1);
                basket.setTotal(basket.getTotal() - p.getPrice());
                basket.deleteProduct(p);
            }
        }
        return basket.getAmount();
    }

    public int countProducts() {
        return basket.getAmount();
    }

    public double getPrice(String name) {
        for (Product p : basket.getProduct()) {
            if (p.getName().equals(name)) {
                return p.getPrice();

            }
        }
        return 0;
    }


    public double getTotalPrice() {
        return basket.getTotal();
    }
}